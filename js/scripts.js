$(function(){
	// Основной слайдер на главной
	$('.main_slider .slider').owlCarousel({
		items: 1,
		margin: 20,
		nav: true,
		dots: true,
		loop: true,
		smartSpeed: 750,
		autoplay: true,
		autoplayTimeout: 5000,
		navText: [
			'<svg><use xlink:href="#arrow_slider" /></svg>',
			'<svg><use xlink:href="#arrow_slider" /></svg>'
		],
		onInitialized: function(event) {
			let items = event.item.count

        	$(event.target).closest('.main_slider').find('.numbers_slider').find('.all').text(items)

        	let page = event.page.index + 2

        	$(event.target).closest('.main_slider').find('.numbers_slider').find('.active').text(page)
		},
		onTranslate: function(event) {
			$(event.target).trigger('stop.owl.autoplay')

        	let page = event.page.index + 1

        	$(event.target).closest('.main_slider').find('.numbers_slider').find('.active').text(page)
		},
	    onTranslated: function(event) {
			$(event.target).trigger('play.owl.autoplay',[5000])
	    }
	})


	// Это интересно
	$('.interesting .slider').owlCarousel({
		loop: true,
		smartSpeed: 500,
	    nav: true,
		dots: false,
		navText: [
			'<svg><use xlink:href="#arrow_slider" /></svg>',
			'<svg><use xlink:href="#arrow_slider" /></svg>'
		],
		responsive: {
	        0:{
	            items: 1,
				margin: 10,
				nav: false
	        },
	        480:{
	            items: 1,
				margin: 20,
				nav: false
	        },
	        768:{
	        	items: 2,
				margin: 30,
				nav: true
	        },
	        1024:{
	        	items: 2,
				margin: 30,
				nav: true
	        },
	        1200:{
	        	items: 2,
				margin: 30,
				nav: true
	        },
	        1260:{
	        	items: 2,
				margin: 86,
				nav: true
	        }
		}
	})



	$('.slider_img').owlCarousel({
		items: 1,
		margin: 20,
		nav: true,
		dots: false,
		loop: true,
		smartSpeed: 500,
		navText: [
			'<svg><use xlink:href="#arrow_slider" /></svg>',
			'<svg><use xlink:href="#arrow_slider" /></svg>'
		]
	})


	// Добавление/Удаление товара в избранное/из избранного
	$('body').on('click', '.favorite_btn button', function(e) {
    	e.preventDefault()

		if( $(this).hasClass('active') ) {
			$(this).removeClass('active')
			// Здесь запрос на удаление товара из избранного
		} else {
			$(this).addClass('active')

			// Здесь запрос на добавление товара в избранное
		}
    })


	if ( $(window).width() > 767 ) {
		//Плавающий блок
		$("#stick_box").stick_in_parent({
			parent: '.stick_stop',
			offset_top: 20
		})
	}


	//Ползунки
	$priceRange = $(".filter #price_range").ionRangeSlider({
        type     : 'double',
        min      : 0,
        max      : 30000,
        from     : 498,
        to       : 26513,
        step     : 1,
        onChange : function (data) {
            $('.filter .price_range input.ot').val( data.from.toLocaleString() )
            $('.filter .price_range input.do').val( data.to.toLocaleString() )
        }
    }).data("ionRangeSlider")

    $('.filter .price_range .input').keyup(function() {
        $priceRange.update({
            from : parseInt( $('.filter .price_range input.ot').val() ),
            to : parseInt( $('.filter .price_range input.do').val() )
        })
    })
})

$(window).load(function(){
	if ( $(window).width() > 1024 ) {
		setTimeout(function(){
			// Анимации
			AOS.init({
				offset: 150,
				delay: 200,
				duration: 1200,
				once: true
			})
		}, 200)
	}

	// Это интересно
	$('.products .slider').owlCarousel({
		loop: false,
		smartSpeed: 500,
	    nav: true,
		dots: false,
		navText: [
			'<svg><use xlink:href="#arrow_slider" /></svg>',
			'<svg><use xlink:href="#arrow_slider" /></svg>'
		],
		onInitialized: function(event){
			setTimeout( function() {
				$(event.target).find('.thumb').height('auto')
				$(event.target).find('.name').height('auto')

				setHeight( $(event.target).find('.thumb') )
				setHeight( $(event.target).find('.name') )
			}, 100)
		},
		onResized: function(event){
			$(event.target).find('.thumb').height('auto')
			$(event.target).find('.name').height('auto')

			setHeight( $(event.target).find('.thumb') )
			setHeight( $(event.target).find('.name') )
		},
		responsive: {
	        0:{
	            items: 2,
				margin: 10,
				nav: false
	        },
	        480:{
	            items: 2,
				margin: 20,
				nav: false
	        },
	        768:{
	        	items: 3,
				margin: 20,
				nav: true
	        },
	        1024:{
	        	items: 4,
				margin: 30
	        }
		}
	})


	// Распродажа
	$('.sell_out .slider').owlCarousel({
		loop: true,
		smartSpeed: 500,
	    nav: true,
		dots: false,
		navText: [
			'<svg><use xlink:href="#arrow_slider" /></svg>',
			'<svg><use xlink:href="#arrow_slider" /></svg>'
		],
		onInitialized: function(event){
			setTimeout( function() {
				$(event.target).find('.slide').height('auto')

				setHeight( $(event.target).find('.slide') )
			}, 100)

			observer.observe()
		},
		onResized: function(event){
			$(event.target).find('.slide').height('auto')

			setHeight( $(event.target).find('.slide') )
		},
		responsive: {
	        0:{
	            items: 1,
				margin: 10,
				autoWidth: false,
				nav: false
	        },
	        480:{
	            items: 1,
				margin: 20,
				autoWidth: true,
				nav: false
	        },
	        768:{
	        	items: 2,
				margin: 20,
				autoWidth: true,
				nav: true
	        },
	        1024:{
	        	items: 2,
				margin: 30,
				autoWidth: true,
				nav: true
	        }
		}
	})

	if ( $(window).width() > 767 ) {
		// Отзывы
		$('.reviews .slider').owlCarousel({
			loop: true,
			smartSpeed: 500,
		    nav: true,
			dots: false,
			navText: [
				'<svg><use xlink:href="#arrow_slider" /></svg>',
				'<svg><use xlink:href="#arrow_slider" /></svg>'
			],
			onInitialized: function(event){
				setTimeout( function() {
					$(event.target).find('.slide').height('auto')
					$(event.target).find('.desc').height('auto')

					setHeight( $(event.target).find('.slide') )
					setHeight( $(event.target).find('.desc') )
				}, 100)
			},
			onResized: function(event){
				$(event.target).find('.slide').height('auto')
				$(event.target).find('.desc').height('auto')

				setHeight( $(event.target).find('.slide') )
				setHeight( $(event.target).find('.desc') )
			},
			responsive: {
		        0:{
		            items: 1,
					margin: 10,
					nav: false
		        },
		        480:{
		            items: 1,
					margin: 20,
					nav: false
		        },
		        768:{
		        	items: 2,
					margin: 15,
					nav: true
		        },
		        1024:{
		        	items: 2,
					margin: 30,
					nav: true
		        }
			}
		})
	}

	// Выравнивание элементов в сетке
	$('.products .grid').each(function() {
		productHeight($(this), parseInt($(this).css('--products_count')))
	})

	// Выравнивание элементов в сетке
	$('.products .main_grid').each(function() {
		productHeight($(this), parseInt($(this).css('--products_count')))
	})


	// Удалить товара из избранных
	$('body').on('click', '.products .product .favorite_delete', function(e) {
    	e.preventDefault()

		$(this).closest('.product').remove()

		$('.products .grid').each(function() {
			productHeight($(this), parseInt($(this).css('--products_count')))
		})
	})


    // Слайдер товара
	$('.product_info .slider').wheelSlider({
		controls: false,
	    dots: false,
	    items: 5,
	    onSlideAfter   : function() {
			$(this).find('iframe').each(function() {
				$(this)[0].contentWindow.postMessage('{"event":"command", "func":"pauseVideo", "args":""}', '*')
			})

			$(this).find('.play_btn').fadeIn(300);
	    },
	})

	// Запуск видео
	$('body').on('click', '.product_info .slide .play_btn', function(e) {
    	e.preventDefault()

    	let parent = $(this).closest('.slide')

    	if ($(this).hasClass('played')) {
    		$(this).fadeOut(300);

    		parent.find('iframe')[0].contentWindow.postMessage('{"event":"command", "func":"playVideo", "args":""}', '*')
    	} else{
			$(this).addClass('played').fadeOut(300);
			parent.find('.preview').fadeOut(300);

			let videoData = $(this).data('video')

			parent.find('iframe').attr("src", videoData+'?autoplay=1;rel=0;enablejsapi=1&amp')
    	}

    })
})


$(window).resize(function(){
	// Выравнивание элементов в сетке
	$('.products .grid').each(function() {
		productHeight($(this), parseInt($(this).css('--products_count')))
	})

	// Выравнивание элементов в сетке
	$('.products .main_grid').each(function() {
		productHeight($(this), parseInt($(this).css('--products_count')))
	})
})


// Выравнивание решений
function productHeight(context, step) {
	let start    = 0
	let finish   = step
	let products = context.find('.product')

	products.find('.thumb').height('auto')
	products.find('.name').height('auto')

	for (let i = 0; i < products.length; i++) {
		setHeight(products.slice(start, finish).find('.thumb'))
		setHeight(products.slice(start, finish).find('.name'))

		start  = start + step
		finish = finish + step
	}
}