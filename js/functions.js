$(function(){
	// Проверка браузера
	if ( !supportsCssVars() ) {
		$('body').addClass('lock')
		$('.supports_error').addClass('show')
	}

	// Ленивая загрузка
	setTimeout(function(){
		observer = lozad('.lozad:not(.loaded)', {
			rootMargin: '200px 0px',
			threshold: 0,
			loaded: function(el) {
				el.classList.add('loaded')
			}
		})

		observer.observe()
	}, 200)


	// Установка ширины стандартного скроллбара
	$(':root').css('--scroll_width', widthScroll() +'px')


	// Моб. меню
	$('body').on('click', '.mob_menu_link', function(e) {
    	e.preventDefault()

		if( $(this).hasClass('active') ) {
			$(this).removeClass('active')

			$('body').removeClass('lock_menu')

			$('header').fadeOut()

		} else {
			$(this).addClass('active')

			$('header').fadeIn()

			$('body').addClass('lock_menu')
		}
    })

    if ( $(window).width() < 1025 ) {
    	$('body').on('click', 'header .open_cats', function(e) {
	    	e.preventDefault()

			if( $(this).hasClass('active') ) {
				$(this).removeClass('active')

				$(this).next().removeClass('show')

				$('header .title_head, header .search, header .my_office.mob, header .info, header .links.mob').removeClass('hide')
			} else {
				$(this).addClass('active')

				$(this).next().addClass('show')

				$('header .title_head, header .search, header .my_office.mob, header .info, header .links.mob').addClass('hide')
			}
	    })

	    $('body').on('click', 'header .cats .list .item > a.sub_link', function(e) {
	    	e.preventDefault()

			if( $(this).hasClass('active') ) {
				$(this).closest('.item').removeClass('active')
				$(this).closest('.list').find('.item').removeClass('hide')

				$('header .cats .bg').removeClass('hide_pattern')

				$(this).removeClass('active')

				$(this).next().removeClass('show')

				$('header .open_cats.active').removeClass('hide')
			} else {
				$(this).closest('.item').addClass('active')
				$(this).closest('.list').find('.item').addClass('hide')

				$('header .cats .bg').addClass('hide_pattern')

				$(this).addClass('active')

				$(this).next().addClass('show')

				$('header .open_cats.active').addClass('hide')
			}
	    })
    }


    // Табы
	var locationHash = window.location.hash

	$('body').on('click', '.tabs button', function(e) {
		e.preventDefault()

		if (!$(this).hasClass('active')) {
			let parent    = $(this).closest('.tabs_container')
			let activeTab = $(this).data('content')
			let level     = $(this).data('level')

			parent.find('.tabs:first button').removeClass('active')
			parent.find('.tab_content.' + level).removeClass('active')

			$(this).addClass('active')
			$(activeTab).addClass('active')
		}
	})

	if (locationHash && $('.tabs_container').length) {
		let activeTab = $('.tabs button[data-content=' + locationHash + ']')
		let parent    = activeTab.closest('.tabs_container')
		let level     = activeTab.data('level')

		parent.find('.tabs:first button').removeClass('active')
		parent.find('.tab_content.' + level).removeClass('active')

		activeTab.addClass('active')
		$(locationHash).addClass('active')

		$('html, body').stop().animate({
			scrollTop: $(locationHash).offset().top
		}, 1000)
	}


	// Всплывающие окна
	$('.modal_link').click(function(e){
		e.preventDefault()

		$.fancybox.close()

		$.fancybox.open({
			src  : $(this).data('content'),
			type : 'inline',
			opts : {
				touch : false,
				speed : 300,
				backFocus : false,
				trapFocus : false,
				autoFocus : false,
				mobile : {
				    clickSlide: "close"
				}
			}
		})
	})


	// Скрыть модалку
	$('.close_modal').click(function(e){
		e.preventDefault()

		$.fancybox.close()
	})

	// Маска ввода
	$('input[type=tel]').inputmask('+7 (999) 999-99-99')

	// Кастомный select
	$('.select_wrap select').niceSelect()


	// Звездочки отзыва
	$('input.wow').rating();


	// Мини всплывающие окна
	firstClick = false

	$('.mini_modal_link').click(function(e){
	    e.preventDefault()

	    let modalId = $(this).data('modal-id')

	    if( $(this).hasClass('active') ){
	        $(this).removeClass('active')
	      	$('.mini_modal').removeClass('active')

	        firstClick = false

			if( $(window).width() < 1024 ){
				$('body').css('cursor', 'default')
			}

			$('body').removeClass('lock')

			$('.overlay').fadeOut(200)
	    }else{
	        $('.mini_modal_link').removeClass('active')
	        $(this).addClass('active')

	        $('.mini_modal').removeClass('active')
	        $(modalId).addClass('active')

	        firstClick = true

			if( $(window).width() < 1024 ){
				$('body').css('cursor', 'pointer')
			}

			if ( $(modalId).hasClass('modal_aside') ) {
				$('body').addClass('lock')

				$('.overlay').fadeIn(200)
			}
	    }
	})

	// Закрываем всплывашку при клике за её пределами
	$(document).click(function(e){
	    if ( !firstClick && $(e.target).closest('.mini_modal').length == 0 ){
	    	if( $('.mini_modal_link').hasClass('active') ){
				$('.overlay').fadeOut(200)

				$('body').removeClass('lock')
			}

	        $('.mini_modal, .mini_modal_link').removeClass('active')

			if( $(window).width() < 1024 ){
				$('body').css('cursor', 'default')
			}
	    }

	    firstClick = false
	})

	// Закрываем всплывашку при клике на крестик во всплывашке
	$('body').on('click', '.mini_modal .close', function(e) {
	    e.preventDefault()

	    $('.mini_modal, .mini_modal_link').removeClass('active')

	    if( $(window).width() < 1024 ){
			$('body').css('cursor', 'default')
		}

		$('.overlay').fadeOut(200)

		$('body').removeClass('lock')

	    firstClick = false
	})


	// Восстановление пароля
	$('body').on('click', '.modal_forgot .submit_btn.send', function(e) {
	    e.preventDefault()

	    let parent = $(this).closest('.modal_forgot')

	    $(this).attr('disabled', 'true');
	    parent.find('.cancel').hide();
	    parent.find('.hidden_fields').slideDown();
	})


	// Окно успешной отправки
	$('body').on('submit', 'form.success_modal', function(e) {
	    e.preventDefault()

	    let parent = $(this).closest('.modal_forgot')

        $('#modal_success').addClass('active')

    	$('.mini_modal, .mini_modal_link').removeClass('active')

        setTimeout( function() {
        	$('#modal_success').removeClass('active')

			$('body').removeClass('lock compensate-for-scrollbar')

			$('.overlay').fadeOut(200)
        }, 3000)


        // Ниже пишем функции отправки данных
	})


	// Закрыть Окно успешной отправки
	$('body').on('click', '.modal_success .close, .overlay', function(e) {
	    e.preventDefault()

    	$('#modal_success').removeClass('active')

		$('.overlay').fadeOut(200)

		$('body').removeClass('lock compensate-for-scrollbar')
	})


	// Отправка форм
	$('body').on('submit', '.form.ajax_submit', function(e) {
		e.preventDefault()

		$.fancybox.close()

		$.fancybox.open({
			src  : '#success_modal2',
			type : 'inline',
			opts : {
				touch : false,
				speed : 300,
				backFocus : false,
				trapFocus : false,
				autoFocus : false,
				mobile : {
				    clickSlide: "close"
				}
			}
		})
	})


	// Изменение количества товара
    $('body').on('click', '.amount .minus', function(e) {
	    e.preventDefault()

	    let parent = $(this).closest('.amount')
	    let input = parent.find('input')
	    let inputVal = parseFloat( input.val() )
	    let minimum = parseFloat( input.data('minimum') )
	    let step = parseFloat( input.data('step') )

	    if( inputVal > minimum ){
	    	input.val( inputVal-step )
	    }
	})

	$('body').on('click', '.amount .plus', function(e) {
	    e.preventDefault()

	    let parent = $(this).closest('.amount')
	    let input = parent.find('input')
	    let inputVal = parseFloat( input.val() )
	    let maximum = parseFloat( input.data('maximum') )
	    let step = parseFloat( input.data('step') )

	    if( inputVal < maximum ){
	    	input.val( inputVal+step )
	    }
	})
})

function setHeight(className){
    let maxheight = 0
    let object = $(className)

    object.each(function() {
    	let elHeight = $(this).innerHeight()

        if( elHeight > maxheight ) {
        	maxheight = elHeight
        }
    })

    object.innerHeight( maxheight )
}

// Вспомогательные функции
function widthScroll() {
    let div = document.createElement('div')
    div.style.overflowY = 'scroll'
    div.style.width = '50px'
    div.style.height = '50px'
    div.style.visibility = 'hidden'
    document.body.appendChild(div)

    let scrollWidth = div.offsetWidth - div.clientWidth
    document.body.removeChild(div)

    return scrollWidth
}


var supportsCssVars = function() {
    var s = document.createElement('style'),
        support

    s.innerHTML = ":root { --tmp-var: bold; }"
    document.head.appendChild(s)
    support = !!(window.CSS && window.CSS.supports && window.CSS.supports('font-weight', 'var(--tmp-var)'))
    s.parentNode.removeChild(s)

    return support
}